#!/bin/bash

#SBATCH --time=00:35:00   # walltime
#SBATCH --nodes=1   # number of processor cores (i.e. tasks)
#SBATCH --mem-per-cpu=1024M   # memory per CPU core
#SBATCH --qos=standby
#SBATCH --gres=gpu:1

# Compatibility variables for PBS. Delete if not needed.
export PBS_NODEFILE=`/fslapps/fslutils/generate_pbs_nodefile` 
export PBS_JOBID=$SLURM_JOB_ID
export PBS_O_WORKDIR="$SLURM_SUBMIT_DIR" 
export PBS_QUEUE=batch

echo running cudaHotplate
echo DEVICES $CUDA_VISIBLE_DEVICES
./cudaHotplate

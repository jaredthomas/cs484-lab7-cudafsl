#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define BLOCKSIZE 1024
#define MAXIT 400
#define TOTROWS		(BLOCKSIZE*8)
#define TOTCOLS		(BLOCKSIZE*8)
#define NOTSETLOC   0 	// for cells that are not fixed
#define SETLOC		1 	// for cells that are fixed
#define MAXERROR	0.1

#define QMAX(x,y) (((x) > (y))? (x): (y))


int *lkeepgoing;
float *iplate;
float *oplate;
int *fixed;	// should maybe be an int or boolean?
float *tmp;
int ncols, nrows;

double When();
void Compute();


int main(int argc, char *argv[])
{
	double t0, tottime;
	ncols = TOTCOLS;
	nrows = TOTROWS;

	cudaMalloc((void **) &lkeepgoing, nrows * ncols * sizeof(int));
	cudaMalloc((void **) &iplate, nrows * ncols * sizeof(float));
	cudaMalloc((void **) &oplate, nrows * ncols * sizeof(float));
	cudaMalloc((void **) &fixed,  nrows * ncols * sizeof(int));
	fprintf(stderr,"Memory allocated\n");
	//fprintf(stderr, "line: %i\n", __LINE__);
	t0 = When();
	/* Now proceed with the Jacobi algorithm */
	//fprintf(stderr, "line: %i\n", __LINE__);
	Compute();
	//fprintf(stderr, "line: %i\n", __LINE__);
	tottime = When() - t0;
	printf("Total Time is: %lf sec.\n", tottime);

	return 0;
}

__global__ void InitArrays(float *ip, float *op, int *fp, int *kp, int ncols)
{
	//if (threadIdx.x*blockIdx.x == 0) {printf("line: %i\n", __LINE__);}
	int i;
	float *oppos, *ippos;
	int  *fppos, *kppos;
	int blockOffset;
	int rowStartPos;
	int colsPerThread;
	//extern __shared__ int row[TOTCOLS];
	
	// Each block gets a row, each thread will fill part of a row

	// Calculate the offset of the row
	blockOffset = blockIdx.x * ncols;
	// Calculate our offset into the row
	rowStartPos = threadIdx.x * (ncols/blockDim.x);
	// The number of cols per thread
	colsPerThread = ncols/blockDim.x;

	ippos = ip + blockOffset + rowStartPos;
	fppos = fp + blockOffset + rowStartPos;
	oppos = op + blockOffset + rowStartPos;
	kppos = kp + blockOffset + rowStartPos;

	// Set initial values
	for (i = 0; i < colsPerThread; i++) {
		fppos[i] = NOTSETLOC; // Not Fixed
		ippos[i] = 50.;
		oppos[i] = 50.;
		kppos[i] = 1; // Keep Going
	}
	
	// Set top row values to 0 degrees and declare as fixed
	if (blockOffset == 0) {
		for (i = 0; i < colsPerThread; i++) {
			fppos[i] = SETLOC; // Fixed
			ippos[i] = 0.;
			oppos[i] = 0.;
		}
	}
	// Set bottom row values to 100 degrees and declare as fixed
	if (blockOffset == ncols-1) {
		for (i = 0; i < colsPerThread; i++) {
			fppos[i] = SETLOC; // Fixed
			ippos[i] = 100.;
			oppos[i] = 100.;
		}
	}
	
	// Set left and right column values to 0 degrees and declare as fixed
	if (threadIdx.x == 0) {
		fppos[0] = SETLOC; // Fixed
		ippos[0] = 0.;
		oppos[0] = 0.;
	}
	
	if (threadIdx.x == blockDim.x-1) {
		fppos[colsPerThread-1] = SETLOC; // Fixed
		ippos[colsPerThread-1] = 0.;
		oppos[colsPerThread-1] = 0.;
	}
		
	// Set [400, 0:330] values to 100 degrees and declare as fixed
	if (blockIdx.x == 0 && threadIdx.x == 0){
		for (i = 0; i < 331; i++){
			fp[400*ncols+i] = SETLOC;
			ip[400*ncols+i] = 100.;
			op[400*ncols+i] = 100.;
		}
	}
	// Set [200, 500] value to 100 degrees and declare as fixed
	if (blockIdx.x == 0 && threadIdx.x == 0){
		fp[200*ncols+500] = SETLOC;
		ip[200*ncols+500] = 100.;
		op[200*ncols+500] = 100.;	
		
		//for (int i = 0; i < TOTCOLS*TOTROWS; i++){
			//if (fp[i] == SETLOC){
			//	printf("%i, %f", i, ip[i]);
			//}
		//}
	}
	
	//if (threadIdx.x == 0){
	//	printf("block %i, thread %i\n", blockIdx.x, threadIdx.x);
	//}
}
__global__ void doCalc(float *ip, float *op, int *fp, int ncols)
{
	//if (threadIdx.x*blockIdx.x == 0) {printf("line: %i\n", __LINE__);}
	
	/* Compute the 5 point stencil for my region */
	int i;
	float *oppos, *ippos;
	int  *fppos;
	int blockOffset;
	int rowStartPos;
	int colsPerThread;
	
	// Calculate the offset of the row
	blockOffset = blockIdx.x * ncols;
	// Calculate our offset into the row
	rowStartPos = threadIdx.x * (ncols/blockDim.x);
	// The number of cols per thread
	colsPerThread = ncols/blockDim.x;
	
	ippos = ip + blockOffset + rowStartPos;
	fppos = fp + blockOffset + rowStartPos;
	oppos = op + blockOffset + rowStartPos;
	
	for (i = 0; i < colsPerThread; i++ ){
		if (*(fppos + i) == NOTSETLOC){
			ippos[i] = (*(oppos + ncols + i) + *(oppos - ncols + i) + *(oppos + i + 1) + *(oppos + i - 1) + 4.*(*(oppos + i)))/8.;
		}
	}
	       
}

__global__ void doCheck(float *ip, float *op, int *fp, int *kp, int ncols)
{
	//if (threadIdx.x*blockIdx.x == 0) {printf("line: %i\n", __LINE__);}
	
	//if (threadIdx.x == blockIdx.x == 0) {printf("line: %i\n", __LINE__);}
	// Calculate keepgoing array
	int i;
	float error = 1000.;
	float *ippos;
	int  *fppos, *kppos;
	int blockOffset;
	int rowStartPos;
	int colsPerThread;
	
	// Calculate the offset of the row
	blockOffset = blockIdx.x * ncols;
	// Calculate our offset into the row
	rowStartPos = threadIdx.x * (ncols/blockDim.x);
	// The number of cols per thread
	colsPerThread = ncols/blockDim.x;
	
	ippos = ip + blockOffset + rowStartPos;
	fppos = fp + blockOffset + rowStartPos;
	kppos = kp + blockOffset + rowStartPos;
	
	for (i = 0; i < colsPerThread; i++ ){
		kppos[i] = 0;
		if (*(fppos + i) == NOTSETLOC){
			error = fabs(ippos[i] - (ippos[i+1] + ippos[i-1] + ippos[i+ncols] + ippos[i-ncols])/4.);
			//printf("error = %f", error);
			kppos[i] = 1;
			if (error < MAXERROR){
				kppos[i] = 0;
			}
			
		}
	}
	
}

__global__ void reduceSum(int *idata, int *odata, unsigned int ncols)
{
	//if (threadIdx.x*blockIdx.x == 0) {printf("line: %i\n", __LINE__);}
	// Reduce rows to the first element in each row
	int i;
	int blockOffset;
	int rowStartPos;
	int colsPerThread;
	int *mypart;

	// Each block gets a row, each thread will reduce part of a row

	// Calculate the offset of the row
    blockOffset = blockIdx.x * ncols;
    // Calculate our offset into the row
	rowStartPos = threadIdx.x * (ncols/blockDim.x);
    // The number of cols per thread
    colsPerThread = ncols/blockDim.x;

	mypart = idata + blockOffset + rowStartPos;

	// Sum all of the elements in my thread block and put them 
    // into the first column spot
	for (i = 1; i < colsPerThread; i++) {
		mypart[0] += mypart[i];
	}
	__syncthreads(); // Wait for everyone to complete
        // Now reduce all of the threads in my block into the first spot for my row
	if(threadIdx.x == 0) {
		odata[blockIdx.x] = 0;
		for(i = 0; i < blockDim.x; i++) {
			odata[blockIdx.x] += mypart[i*colsPerThread];
		}
		//printf("odata[%i] = %i, i = %i\n", blockIdx.x, odata[blockIdx.x], i);
	}
	
	// this may not be necessary
	//for (i = 1; i < colsPerThread; i++) {
	//	mypart[i] = 0;
	//}	
	
	// We cant synchronize between blocks, so we will have to start another kernel
}

__global__ void reduceSumSequentialAddressing(int *idata, int *odata, unsigned int ncols)
{
	//if (threadIdx.x*blockIdx.x == 0) {printf("line: %i\n", __LINE__);}
	// Reduce rows to the first element in each row
	int i;
	int blockOffset;
	int rowStartPos;
	int colsPerThread;
	int *ourpart;

	// Each block gets a row, each thread will reduce part of a row

	// Calculate the offset of the row
    blockOffset = blockIdx.x * ncols;
    // Calculate our offset into the row
	rowStartPos = threadIdx.x * (ncols/blockDim.x);
    // The number of cols per thread
    colsPerThread = ncols/blockDim.x;

	ourpart = idata + blockOffset;
	
	// Sum all of the elements in my thread block and put them 
    // into the first column spot
	for (i = threadIdx.x+blockDim.x; i < ncols; i+=blockDim.x) {
		ourpart[threadIdx.x] += ourpart[i];
	}
	__syncthreads(); // Wait for everyone to complete
    
    // Now reduce all of the threads in my block into the first spot for my row
	int tid = threadIdx.x;
        if (tid < 512) { ourpart[tid] += ourpart[tid+512];}  
        __syncthreads();
        if (tid < 256) { ourpart[tid] += ourpart[tid+256];}
        __syncthreads();
        if (tid < 128) { ourpart[tid] += ourpart[tid+128];}
        __syncthreads();
        if (tid < 64) { ourpart[tid] += ourpart[tid+64];}
        __syncthreads();
        if (tid < 32) { ourpart[tid] += ourpart[tid+32];}
        __syncthreads();
		if (tid < 16) { ourpart[tid] += ourpart[tid+16];}
        __syncthreads();
		if (tid < 8) { ourpart[tid] += ourpart[tid+8];}
        __syncthreads();
		
	if(threadIdx.x == 0) {
		odata[blockIdx.x] = 0;
		for(i = 0; i < 8; i++) {
			odata[blockIdx.x] += ourpart[i];
		}
	}
}

/* __global__ void reduceSingle(int *idata, int *single, int nrows)
{
	//if (threadIdx.x*blockIdx.x == 0) {printf("line: %i\n", __LINE__);}
	
	//printf("line: %i\n", __LINE__);
	//printf("\nkernel\n");
	//return;
	//if (threadIdx.x*blockIdx.x == 0) {printf("line: %i\n", __LINE__);}
	// Reduce rows to the first element in each row
	int i;
	//int rowStartPos;
	//int colsPerThread;
	
	if(threadIdx.x == 0) {
		*single = 0;
		for(i = 0; i < nrows; i++) {
			printf("[%d]=%d+%d\n",i, *single, idata[i]);
			*single += idata[i];
		}
		printf("single: %i\n",*single);
	}
}
 */


 __global__ void reduceSingleInterleavedAddressing1(int *idata, int *single, int nrows)
{
	// Reduce rows to the first element in each row
	int i;
    int rowStartPos;
    int colsPerThread;
	extern __shared__ int parts[];
	
    // Each block gets a row, each thread will reduce part of a row

    // Calculate our offset into the row
	rowStartPos = threadIdx.x * (nrows/blockDim.x);
    // The number of cols per thread
    colsPerThread = nrows/blockDim.x;

	// Sum my part of one dimensional array and put it shared memory
	parts[threadIdx.x] = 0;
	for (i = rowStartPos; i < rowStartPos+colsPerThread; i++) {
		parts[threadIdx.x] += idata[i];
	}
	
	for (unsigned int s=1; s<blockDim.x; s *= 2){
		if (threadIdx.x % (2*s) == 0){
			parts[threadIdx.x] += parts[threadIdx.x+s];
		}
		__syncthreads();
	}
	
	
	if(threadIdx.x == 0) {
		*single = parts[0];
	}	
	
}

__global__ void reduceSingleInterleavedAddressing2(int *idata, int *single, int nrows)
{
	// Reduce rows to the first element in each row
	int i;
    int rowStartPos;
    int colsPerThread;
	extern __shared__ int parts[];
	
    // Each block gets a row, each thread will reduce part of a row

    // Calculate our offset into the row
	rowStartPos = threadIdx.x * (nrows/blockDim.x);
    // The number of cols per thread
    colsPerThread = nrows/blockDim.x;

	// Sum my part of one dimensional array and put it shared memory
	parts[threadIdx.x] = 0;
	for (i = rowStartPos; i < rowStartPos+colsPerThread; i++) {
		parts[threadIdx.x] += idata[i];
	}
	
	for (unsigned int s=1; s<blockDim.x; s *= 2){
		int index = 2*s*threadIdx.x;
		if (index<blockDim.x){
			parts[index] += parts[index+s];
		}
		__syncthreads();
	}
	
	if(threadIdx.x == 0) {
		*single = parts[0];
	}	
}


 __global__ void reduceSingleSequentialAddressing(int *idata, int *single, int nrows)
{
	// Reduce rows to the first element in each row
	int i;
    int rowStartPos;
    int colsPerThread;
	extern __shared__ int parts[];
	
    // Each block gets a row, each thread will reduce part of a row

    // Calculate our offset into the row
	rowStartPos = threadIdx.x * (nrows/blockDim.x);
    // The number of cols per thread
    colsPerThread = nrows/blockDim.x;

	// Sum my part of one dimensional array and put it shared memory
	parts[threadIdx.x] = 0;
	for (i = threadIdx.x; i < nrows; i+=blockDim.x) {
		parts[threadIdx.x] += idata[i];
	}
	int tid = threadIdx.x;
        if (tid < 512) { parts[tid] += parts[tid + 512];}  
        __syncthreads();
        if (tid < 256) { parts[tid] += parts[tid + 256];}
        __syncthreads();
        if (tid < 128) { parts[tid] += parts[tid + 128];}
        __syncthreads();
        if (tid < 64) { parts[tid] += parts[tid + 64];}
        __syncthreads();
        if (tid < 32) { parts[tid] += parts[tid + 32];}
        __syncthreads();
		if (tid < 16) { parts[tid] += parts[tid + 16];}
        __syncthreads();
		if (tid < 8) { parts[tid] += parts[tid + 8];}
        __syncthreads();
	if(threadIdx.x == 0) {
		*single = 0;
		for(i = 0; i < 8; i++) {
			*single += parts[i];
		}
	}
}

	
void Compute()
{
	//fprintf(stderr, "line: %i\n", __LINE__);
	int *keepgoing_single;
	int *keepgoing_sums;
	int keepgoing;
	int blocksize = BLOCKSIZE;
	int iteration;
	//double t0, tottime;

	ncols = TOTCOLS;
	nrows = TOTROWS;

	// One block per row
	InitArrays<<< nrows, blocksize >>>(iplate, oplate, fixed, lkeepgoing, ncols);
	cudaDeviceSynchronize();
	//fprintf(stderr, "line: %i\n", __LINE__);
	cudaMalloc((void **)&keepgoing_single, 1 * sizeof(int));
	keepgoing = 1;
	//fprintf(stderr, "line: %i\n", __LINE__);
	cudaMalloc((void **)&keepgoing_sums, nrows * sizeof(int));
 	int *peek = (int *)malloc(nrows*sizeof(int));
	//fprintf(stderr, "line: %i\n", __LINE__);
	for (iteration = 0; (iteration < MAXIT) && keepgoing; iteration++)
	{
		//fprintf(stderr, "line: %i\n", __LINE__);
		doCalc<<< nrows, blocksize >>>(iplate, oplate, fixed, ncols);
		//cudaDeviceSynchronize();
		//fprintf(stderr, "line: %i\n", __LINE__);
		doCheck<<< nrows, blocksize >>>(iplate, oplate, fixed, lkeepgoing, ncols);
		//cudaDeviceSynchronize();
		cudaError_t error = cudaGetLastError();
		if(error != cudaSuccess)
		{
			printf("CUDA error 0: %s\n", cudaGetErrorString(error));
			exit(-1);
		}
		//fprintf(stderr, "line: %i\n", __LINE__);
		//reduce3SeqAddressing_sums<<< nrows, blocksize, blocksize*sizeof(int) >>>(lkeepgoing, keepgoing_sums, nrows, ncols);
		//cudaDeviceSynchronize();
		error = cudaGetLastError();
		if(error != cudaSuccess)
		{
			printf("CUDA error 1: %s\n", cudaGetErrorString(error));
			exit(-1);
		}
		
		//reduceSum<<< nrows, blocksize>>>(lkeepgoing, keepgoing_sums, ncols);
		reduceSumSequentialAddressing<<< nrows, blocksize>>>(lkeepgoing, keepgoing_sums, ncols);
		
		cudaMemcpy(peek, keepgoing_sums, nrows*sizeof(int), cudaMemcpyDeviceToHost);
		//fprintf(stderr, "after cudaMemcpy \n");
		//int i;
 		//for(i = 0; i < nrows; i++) {
		//	fprintf(stderr, "row %i: %d \n",i, peek[i]);
		//}
		// Now we have the sum for each row in the first column, 
		//  reduce to one value
		//fprintf(stderr, "line: %i\n", __LINE__);
		//reduceSingleInterleavedAddressing1<<<1, blocksize, blocksize*sizeof(int)>>>(keepgoing_sums, keepgoing_single, nrows);
		//reduceSingleInterleavedAddressing2<<<1, blocksize, blocksize*sizeof(int)>>>(keepgoing_sums, keepgoing_single, nrows);
		reduceSingleSequentialAddressing<<<1, blocksize, blocksize*sizeof(int)>>>(keepgoing_sums, keepgoing_single, nrows);
		
		//cudaDeviceSynchronize();
		error = cudaGetLastError();
		if(error != cudaSuccess)
		{
			printf("CUDA error 2: %s\n", cudaGetErrorString(error));
			exit(-1);
		}
		keepgoing = 0;
		//fprintf(stderr, "line: %i\n", __LINE__);
		cudaMemcpy(&keepgoing, keepgoing_single, 1 * sizeof(int), cudaMemcpyDeviceToHost);
		//fprintf(stderr, "keepgoing = %d\n", keepgoing);

		/* swap the new value pointer with the old value pointer */
		tmp = oplate;
		oplate = iplate;
		iplate = tmp;
	}
	free(peek);
	cudaFree(keepgoing_single);
	cudaFree(keepgoing_sums);
	fprintf(stderr,"Finished in %i iterations\n", iteration);
}

/* Return the current time in seconds, using a double precision number.       */
double When()
{
	//fprintf(stderr, "line: %i\n", __LINE__);
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}

/* reduction code pieces
if (tid < 512) { parts[tid] += parts[tid + 512]; }
__syncthreads();
if (tid < 256) { parts[tid] += parts[tid + 256];}
__syncthreads();
etc . . .
if(threadIdx.x == 0) {
	*single == 0;
	for(i = 0; i < 32; i++){
		*single += parts[i];
	}
}
*/

/* interleaved


	}
}
*/